import Vue from "vue";
import App from "./App.vue";
import "./assets/style.css";
import router from "./router";

import store from "./plugins/store";
import auth from "./plugins/auth";
import ContenuAPI from "./plugins/ContenuAPI";

require("dotenv").config({ path: "../.env" });

Vue.use(store);
Vue.use(auth);
Vue.use(ContenuAPI);

new Vue({
  el: "#app",
  router,
  render: h => h(App)
});
