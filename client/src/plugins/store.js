class Store {
  data = undefined;
  setData(data) {
    this.data = data;
  }
  getData() {
    return this.data;
  }
}
export default {
  install(Vue) {
    Vue.prototype.$store = new Store();
  }
};
