const express = require("express");
const app = express();
const path = require("path");
require("dotenv").config({ path: "../.env" });
app.use(express.json());
app.use(require("cors")());
require("./contenu")(app);
if (process.env.MODE.toLowerCase() == "production") {
  app.use("/dist", express.static(path.join(__dirname, "/../client/dist/")));
  app.get("/*", (req, res) => res.sendFile(path.join(__dirname, "/../client/index.html")));
} else {
  let httpProxy = require("http-proxy");
  let apiProxy = httpProxy.createProxyServer();
  app.all("/*", function (req, res) {
    apiProxy.web(req, res, { target: "http://localhost:" + process.env.CLIENT_PORT });
  });
}

app.listen(process.env.SERVER_PORT, () => console.log(`Contenu server listening at http://localhost:${process.env.SERVER_PORT}`));
