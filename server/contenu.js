const api = require("contenu-server-api")
const endPointFunctions = api.endPointFunctions;
module.exports = (router) => {

  router.get("/api/init", endPointFunctions.init);
  router.get("/api/data", endPointFunctions.getData);
  router.post("/api/data", endPointFunctions.setData);
  router.post("/api/credentials", endPointFunctions.credentials);
  router.post("/api/login", endPointFunctions.login);

};
